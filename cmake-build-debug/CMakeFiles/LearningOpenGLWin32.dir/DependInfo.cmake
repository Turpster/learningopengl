# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "D:/dev/Projects/learningopengl/src/Debug.cpp" "D:/dev/Projects/learningopengl/cmake-build-debug/CMakeFiles/LearningOpenGLWin32.dir/src/Debug.cpp.obj"
  "D:/dev/Projects/learningopengl/src/Game.cpp" "D:/dev/Projects/learningopengl/cmake-build-debug/CMakeFiles/LearningOpenGLWin32.dir/src/Game.cpp.obj"
  "D:/dev/Projects/learningopengl/src/Logger.cpp" "D:/dev/Projects/learningopengl/cmake-build-debug/CMakeFiles/LearningOpenGLWin32.dir/src/Logger.cpp.obj"
  "D:/dev/Projects/learningopengl/src/Mesh.cpp" "D:/dev/Projects/learningopengl/cmake-build-debug/CMakeFiles/LearningOpenGLWin32.dir/src/Mesh.cpp.obj"
  "D:/dev/Projects/learningopengl/src/Shader.cpp" "D:/dev/Projects/learningopengl/cmake-build-debug/CMakeFiles/LearningOpenGLWin32.dir/src/Shader.cpp.obj"
  "D:/dev/Projects/learningopengl/src/Transform.cpp" "D:/dev/Projects/learningopengl/cmake-build-debug/CMakeFiles/LearningOpenGLWin32.dir/src/Transform.cpp.obj"
  "D:/dev/Projects/learningopengl/src/Window.cpp" "D:/dev/Projects/learningopengl/cmake-build-debug/CMakeFiles/LearningOpenGLWin32.dir/src/Window.cpp.obj"
  )
set(CMAKE_CXX_COMPILER_ID "MSVC")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "GLEW_STATIC"
  "GLM_ENABLE_EXPERIMENTAL"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../Dependancies/GLEW/include"
  "../Dependancies/GLFW/include"
  "../Dependancies/GLM/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")

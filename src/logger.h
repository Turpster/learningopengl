#pragma once

#include <string>

class Logger
{
public:
	Logger();
	~Logger();

	enum
	{
		LOGGER_LEVEL_INFO,
		LOGGER_LEVEL_NORMAL,
		LOGGER_LEVEL_ERROR,
		LOGGER_LEVEL_CRITICAL,
		LOGGER_LEVEL_WARNING
	};

	void log(unsigned int loggerlevel, const std::string& log) const; 
	void log(unsigned int loggerlevel, const std::string& log, const std::string& error) const;
private:
	inline std::string GetLoggerLevelString(unsigned int loggerlevel) const;
};


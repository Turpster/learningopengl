#include "window.h"
#include "Game.h"
#include <GLEW/glew.h>

Logger Game::logger = Logger();

Window::Window(int width, int height, const std::string& title) : m_width(width), m_height(height)
{
	if (!glfwInit())
		Game::logger.log(Logger::LOGGER_LEVEL_CRITICAL, "Unable to initialise GLFW");

	m_window = glfwCreateWindow(m_width, m_height, title.c_str(), nullptr, nullptr);

	glfwMakeContextCurrent(m_window);

	if (glewInit() != GLEW_OK)
		Game::logger.log(Logger::LOGGER_LEVEL_CRITICAL, "Unable to initialise GLEW");
}


bool Window::WindowIsClosed()
{
	return glfwWindowShouldClose(m_window);
}

void Window::Update()
{
	glfwSwapBuffers(m_window);
	glfwPollEvents();
}

int Window::GetWidth()
{
	return m_width;
}

int Window::GetHeight()
{
	return m_height;
}

void Window::SetHeight(int height)
{
	m_height = height;
}

void Window::SetWidth(int width)
{
	m_width = width;
}


Window::~Window()
{
}

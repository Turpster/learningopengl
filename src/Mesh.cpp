#include "mesh.h"

#include <GLEW/glew.h>

#include <vector>
#include "debug.h"
#include <glm/glm.hpp>

Mesh::Mesh(Vertex* verticies, unsigned int count) : m_count(count)
{
	glc(glGenVertexArrays(1, &m_vao));
	glc(glBindVertexArray(m_vao));

	std::vector<glm::vec3> positions;

	positions.reserve(count);

	for (unsigned int i = 0; i < count; i++)
	{
		positions.emplace_back(verticies[i].pos);
	}

	glc(glGenBuffers(COUNT_VB, &m_vertexarraybuffer[POSITION_VB]));

	glc(glBindBuffer(GL_ARRAY_BUFFER, m_vertexarraybuffer[POSITION_VB]));
	glc(glBufferData(GL_ARRAY_BUFFER, sizeof(positions[0]) * positions.size(), &positions[0], GL_STATIC_DRAW));
	glc(glEnableVertexAttribArray(POSITION_VB));
	glc(glVertexAttribPointer(POSITION_VB, 3, GL_FLOAT, GL_FALSE, 0, 0));

	glc(glBindVertexArray(0));
}

void Mesh::Draw()
{
	glc(glBindVertexArray(m_vao));

	glc(glDrawArrays(GL_TRIANGLES, 0, m_count));

	glc(glBindVertexArray(0));
}


Mesh::~Mesh()
{
	glc(glDeleteBuffers(COUNT_VB, m_vertexarraybuffer));
	glc(glDeleteVertexArrays(1, &m_vao));
}
#include "GLEW/glew.h"
#include "Game.h"
#include "mesh.h"
#include "shader.h"
#include "transform.h"

#define WIDTH 800
#define HEIGHT 600
#define TITLE "OpenGL"


Game::Game() : m_window(Window(WIDTH, HEIGHT, TITLE)) {};

int Game::start()
{
	glClearColor(0.05f, 0.15f, 0.3f, 1.0f);

	Vertex verticies[] = {
		{glm::vec3(-0.5, -0.5, 1.0)},
		{glm::vec3(0.0,  0.5, 1.0)},
		{glm::vec3(0.5, -0.5, 1.0)}
	};

	Mesh mesh(verticies, 3);
	Shader shader("../res/shaders/simple");

	Transform transform;

	shader.Bind();

	while (!m_window.WindowIsClosed())
	{
		glClear(GL_COLOR_BUFFER_BIT);

		shader.Update(transform);

		mesh.Draw();

		m_window.Update();
	}
	return 0;
}

Game::~Game()
{

}

int main()
{
	Game game;
	game.start();
	return 0;
}
